<?php
namespace MILEXA\WPAWESOME\ADDONS\WC;

if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\WC\\Config") ) :
class Config
{
    public static function init(){
        $class = __CLASS__;
        new $class;
    }
    public function __construct(){}


}
endif;