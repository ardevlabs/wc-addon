<?php
namespace MILEXA\WPAWESOME\ADDONS\WC;
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ):
if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\WC\\WC_Setting") ) :
class WC_Setting extends \WC_Settings_Page{
	/**
	 *
	 */
	public static function init(){
        $class = __CLASS__;
        new $class;
    }

    public function __construct(){
        parent::__construct();
    }
}
endif;
endif;